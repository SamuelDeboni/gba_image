const std = @import("std");
const Builder = std.build.Builder;

pub fn build(b: *Builder) void {
    const target = b.standardTargetOptions(.{});
    const mode = b.standardReleaseOptions();
    
    const exe = b.addExecutable("image_creator", "src/main.zig");
    exe.addPackage(.{.name = "pixel_draw", .path = "./packages/pixel_draw/src/pixel_draw.zig"});
    
    
    exe.setTarget(target);
    exe.setBuildMode(mode);
    
    exe.linkLibC();
    exe.linkSystemLibrary("X11");
    
    exe.install();
    
    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }
    
    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
