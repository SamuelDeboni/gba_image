usingnamespace @import("index.zig");

pub fn mouseOnRect(x: i64, y: i64, w: u64, h: u64) bool {
    const mx = draw.mouse_pos_x;
    const my = draw.mouse_pos_y;
    
    const on_rect = mx >= x and my >= y and
        mx < (x + @intCast(i64, w)) and
        my < (y + @intCast(i64, h));
    
    return on_rect;
}

pub fn valueBar(x: i64, y: i64, w: u64, h: u64, value: *f32) void {
    const mx = draw.mouse_pos_x;
    const my = draw.mouse_pos_y;
    
    if (mouseOnRect(x - 4, y, w + 8, h) and draw.mouseButtonPressed(.left)) {
        value.* = @intToFloat(f32, mx - x) / @intToFloat(f32, w);
        value.* = math.clamp(value.*, 0.0, 1.0);
    }
    
    draw.gb.drawRect(x, y, w + 4, h, Color.c(1, 1, 1, 1));
    
    const vx = @floatToInt(i64, value.* * @intToFloat(f32, w)) + x;
    draw.gb.fillRect(vx, y, 4, h, Color.c(1, 1, 1, 1));
}

/// Cria o titulo do menu
pub fn menuHead(label: []const u8, x: *i64, y: *i64, is_selecting: *bool, is_colapsed: *bool) bool {
    const w = 196;
    const h = 32;
    
    const is_mouse_on = draw.mouse_pos_x >= x.* and draw.mouse_pos_y >= y.* and
        draw.mouse_pos_x < x.* + w and draw.mouse_pos_y < y.* + h;
    
    var fill_color = Color.c(0.1, 0.1, 0.1, 1.0);
    var border_color = Color.c(1, 1, 1, 1.0);
    
    if (is_mouse_on) {
        fill_color = Color.c(0.6, 0.2, 0.2, 1.0);
        
        if (draw.mouseButtonDown(.left)) {
            is_selecting.* = true;
        } else if (draw.mouseButtonUp(.left)) {
            is_selecting.* = false;
        }
        if (draw.mouseButtonUp(.right)) {
            is_colapsed.* = !is_colapsed.*;
        }
    }
    
    if (is_selecting.*) {
        x.* = draw.mouse_pos_x - 98;
        y.* = draw.mouse_pos_y - 16;
        
        fill_color = Color.c(0.6, 0.1, 0.1, 1.0);
    }
    
    draw.gb.fillRect(x.*, y.*, w, h, fill_color);
    draw.gb.drawRect(x.*, y.*, w, h, border_color);
    
    draw.gb.drawBitmapFont(label, x.* + 2, y.* + 8, 1, 1, font);
    
    return !is_colapsed.*;
}

pub fn menuHeadFixed(label: []const u8, x: i64, _y: *i64, is_colapsed: *bool) bool {
    const w = 196;
    const h = 32;
    const y = _y.*;
    const is_mouse_on = mouseOnRect(x, y, w, h);
    
    var fill_color = Color.c(0.1, 0.1, 0.1, 1.0);
    var border_color = Color.c(1, 1, 1, 1.0);
    
    if (is_mouse_on) {
        fill_color = Color.c(0.4, 0.1, 0.1, 1.0);
        
        if (draw.mouseButtonUp(.left)) {
            is_colapsed.* = !is_colapsed.*;
        }
    }
    
    draw.gb.fillRect(x, y, w, h, fill_color);
    draw.gb.drawRect(x, y, w, h, border_color);
    
    draw.gb.drawBitmapFont(label, x + 2, y + 8, 1, 1, font);
    
    _y.* += 32;
    
    return !is_colapsed.*;
}

pub fn separator(x: i64, _y: *i64) void {
    const w = 180;
    const h = 8;
    const y = _y.*;
    
    var fill_color = Color.c(0.1, 0.1, 0.1, 1.0);
    var border_color = Color.c(1, 1, 1, 1.0);
    
    draw.gb.fillRect(x, y, w, h, fill_color);
    draw.gb.drawRect(x, y, w, h, border_color);
    
    _y.* += 8;
}

/// Cria um botao
pub fn selectButton(label: []const u8, option: *u32, value: u32, x: i64, y: *i64) void {
    if(doButton(label, font, x, y.*, 180, 24, draw.mouse_pos_x, draw.mouse_pos_y, draw.mouseButtonPressed(.left), draw.mouseButtonUp(.left), option.* == value))
    {
        if (option.* == value) {
            option.* = 0;
        } else {
            option.* = value;
        }
    }
    y.* += 24;
}

pub fn button(label: []const u8, x: i64, y: *i64) bool {
    const result = doButton(label, font, x, y.*, 180, 24, draw.mouse_pos_x, draw.mouse_pos_y, draw.mouseButtonPressed(.left), draw.mouseButtonUp(.left), false);
    y.* += 24;
    return result;
}


pub fn doButton(label: []const u8, f: draw.BitmapFont,
                x: i64, y: i64, w: u64, h: u64, mouse_x: i64, mouse_y: i64, mouse_pressed: bool, mouse_up: bool, toggled: bool) bool
{
    const is_mouse_on_button = mouseOnRect(x, y, w, h);
    
    var fill_color = Color.c(0.1, 0.1, 0.1, 1.0);
    var border_color = Color.c(1, 1, 1, 1.0);
    
    if (toggled) {
        fill_color = Color.c(0.4, 0.4, 0.1, 1.0);
    }
    
    var result = false;
    
    if (is_mouse_on_button) {
        
        if (mouse_up) {
            result = true;
        } else if (mouse_pressed) {
            fill_color = Color.c(0.1, 0.1, 0.3, 1.0);
        } else if (toggled){
            fill_color = Color.c(0.4, 0.4, 0.1, 1.0);
        } else {
            fill_color = Color.c(0.1, 0.1, 0.4, 1.0);
        }
    }
    
    draw.gb.fillRect(x, y, w, h, fill_color);
    draw.gb.drawRect(x, y, w, h, border_color);
    
    draw.gb.drawBitmapFont(label, x + 2, y + 4, 1, 1, f);
    
    return result;
}


pub fn textBox(x: i64, y: i64, w: u64, h: u64, comptime buffer_size: u32, buffer: *draw.util.FixedSizeString(buffer_size), selected: *bool) bool {
    const mouse_on_rect = mouseOnRect(x, y, w, h);
    const mouse_up = draw.mouseButtonUp(.left);
    
    if (mouse_on_rect and mouse_up) {
        selected.* = !selected.*;
        draw.block_key_input = selected.*;
    } else if (mouse_up) {
        selected.* = false;
        draw.block_key_input = false;
    }
    
    var result = false;
    
    var color = Color.c(1, 1, 1, 1);
    var bg_color = Color.c(0.1, 0.1, 0.1, 1);
    
    if (mouse_on_rect) {
        bg_color.b = 0.4;
    }
    
    if (selected.*) {
        color.g = 0.3;
        color.b = 0.2;
        
        var i: usize = 0;
        while (i < draw.char_input_len) : (i += 1) {
            const character = draw.char_input_buffer[i];
            if (character == '\n') {
                result = true;
                selected.* = false;
                break;
            }
            if (buffer.len >= buffer.data.len) break;
            
            if (character >= ' ' and character <= '~') {
                buffer.data[buffer.len] = character;
                buffer.len += 1;
            } else if (character == 0x8 and buffer.len > 0) { // backspace
                buffer.len -= 1;
            }
        }
    }
    
    draw.gb.fillRect(x, y, w, h, bg_color);
    draw.gb.drawRect(x, y, w, h, color);
    draw.gb.drawBitmapFont(buffer.data[0 .. buffer.len], x + 4, y + 2, 1, 1, font_small);
    
    return result;
}