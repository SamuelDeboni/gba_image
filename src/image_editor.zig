usingnamespace @import("index.zig");

const Color16 = packed struct {
    r: u5 = 0,
    g: u5 = 0,
    b: u5 = 0,
    _pad: u1 = 0,
    
    pub fn toColor(col16: Color16) Color {
        return Color{
            .r = @intToFloat(f32, col16.r) / 31.0,
            .g = @intToFloat(f32, col16.g) / 31.0,
            .b = @intToFloat(f32, col16.b) / 31.0,
            .a = 1.0,
        };
    }
};

// ==============================================
var palette = [_] Color16 {.{}} ** 256;
var selected_color_index: u8 = 0;
var selected_color: Color = .{ .a = 1.0 };
pub var font_small: draw.BitmapFont = undefined;
pub var font: draw.BitmapFont = undefined;
pub var current_sprite = Sprite{};
pub var current_screenblock = Sprite{
    .shape = 0,
    .size = 2,
};
// ==============================================


pub const Sprite = struct {
    shape: u32 = 0,
    size: u32 = 0,
    
    tiles: [256][64]u8  = undefined,
    
    pub fn c() Sprite {
        var sprite = Sprite{};
        
        var y: usize = 0;
        while (y < 64) : (y += 1) {
            var x: usize = 0;
            while (x < 64) : (x += 1) {
                sprite.tiles[y][x] = 0;
            }
        }
        
        return sprite;
    }
    
    pub fn tileCount(self: *Sprite) usize {
        if (self.shape == 0) {
            const x_count = @as(u32, 1) << @intCast(u5, self.size);
            return x_count * x_count;
        }
        return 0;
    }
};

pub fn updatePalette(draw_tile: bool) void {
    const plt_w = 16;
    const plt_h = 16;
    const plt_csize = 16;
    
    const plt_x = 32;
    const plt_y = 180 - 24;
    
    if (!draw_tile) { // Draw palette
        var y: usize = 0;
        while (y < plt_h) : (y += 1) {
            var x: usize = 0;
            while (x < plt_w) : (x += 1) {
                const xr = @intCast(i32, x) * plt_csize + plt_x;
                const yr = @intCast(i32, y) * plt_csize + plt_y;
                const index = x + y * plt_w;
                
                draw.gb.fillRect(xr, yr, plt_csize, plt_csize, palette[index].toColor());
            }
        }
        draw.gb.drawRect(plt_x - 1, plt_y - 1, plt_w * plt_csize + 2, plt_h * plt_csize + 2, Color.c(1,1,1,1));
    } else {
        var y: usize = 0;
        while (y < 16) : (y += 1) {
            var x: usize = 0;
            while (x < 16) : (x += 1) {
                const xr = @intCast(i32, x) * plt_csize + plt_x;
                const yr = @intCast(i32, y) * plt_csize + plt_y;
                const index = x + y * 16;
                
                drawTileNotClamp(current_sprite.tiles[index][0..], xr, yr, 2, false);
            }
        }
        draw.gb.drawRect(plt_x - 1, plt_y - 1, plt_w * plt_csize + 2, plt_h * plt_csize + 2, Color.c(1,1,1,1));
    }
    
    { // Select Color
        var mx = @divFloor(draw.mouse_pos_x - plt_x, plt_csize);
        var my = @divFloor(draw.mouse_pos_y - plt_y, plt_csize);
        mx = math.clamp(mx, 0, plt_w - 1);
        my = math.clamp(my, 0, plt_h - 1);
        
        // Draw selector
        const mouse_on_rect = mouseOnRect(plt_x, plt_y, plt_w * plt_csize, plt_h * plt_csize);
        
        if (mouse_on_rect) {
            draw.gb.drawRect(mx * plt_csize + plt_x, my * plt_csize + plt_y, plt_csize, plt_csize, Color.c(1, 1, 1, 1));
            
            if (draw.mouseButtonUp(.left)) {
                selected_color_index = @intCast(u8, mx + my * plt_w);
            } else if (draw.mouseButtonUp(.right)) {
                const index = @intCast(u8, mx + my * plt_w);
                palette[selected_color_index] = palette[index];
            }
            
            selected_color = palette[selected_color_index].toColor();
        }
        
        const sc_x: i32 = selected_color_index % plt_w;
        const sc_y: i32 = selected_color_index / plt_w;
        
        draw.gb.drawRect(sc_x * plt_csize + plt_x, sc_y * plt_csize + plt_y, plt_csize, plt_csize, Color.c(0.8, 0.8, 0.8, 1));
    }
    
    if (!draw_tile) { // Draw Selected Color
        var menu_x: i32 = 32;
        var menu_y: i32 = 32;
        
        const mcolor1 = Color.c(0.1, 0.1, 0.1, 1.0);
        const mcolor2 = Color.c(0.4, 0.4, 0.4, 1.0);
        const mcolor3 = Color.c(0.0, 0.0, 0.0, 1.0);
        const mcolor4 = Color.c(1.0, 1.0, 1.0, 1.0);
        
        // draw panel
        draw.gb.fillRect(menu_x, menu_y, 256, 114, mcolor1);
        draw.gb.drawRect(menu_x, menu_y, 256, 114, mcolor4);
        
        menu_x += 24;
        menu_y += 24;
        
        draw.gb.fillRect(menu_x, menu_y, 64, 64, palette[selected_color_index].toColor());
        draw.gb.drawRect(menu_x - 2, menu_y - 2, 68, 68, mcolor3);
        
        menu_x += 72;
        valueBar(menu_x, menu_y, 128, 10, &selected_color.r);
        menu_y += 16;
        valueBar(menu_x, menu_y, 128, 10, &selected_color.g);
        menu_y += 16;
        valueBar(menu_x, menu_y, 128, 10, &selected_color.b);
        
        palette[selected_color_index] = .{
            .r = @floatToInt(u5, selected_color.r * 31 + 0.5),
            .g = @floatToInt(u5, selected_color.g * 31 + 0.5),
            .b = @floatToInt(u5, selected_color.b * 31 + 0.5),
        };
        
        menu_y += 20;
        
        const color = palette[selected_color_index];
        draw.gb.drawBitmapFontFmt("r: {} g: {} b: {}",
                                  .{color.r, color.g, color.b},
                                  @intCast(u32, menu_x),
                                  @intCast(u32, menu_y),
                                  1, 1, font_small);
    }
}

var edit_sprite_image_x: i64 = 384;
var edit_sprite_image_y: i64 = 32;
var edit_sprite_scale: i64 = 32;
pub fn editSprite(sprite: *Sprite, is_screen_block: bool) void {
    var image_x: i64 = edit_sprite_image_x;
    var image_y: i64 = edit_sprite_image_y;
    
    if (draw.keyDown(.up) and edit_sprite_scale < 32)
        edit_sprite_scale *= 2;
    if (draw.keyDown(.down) and edit_sprite_scale > 4)
        edit_sprite_scale = @divFloor(edit_sprite_scale, 2);
    
    var scale: i64 = edit_sprite_scale;
    if (is_screen_block and scale == 4) scale = 8;
    
    if (draw.keyPressed(.w)) edit_sprite_image_y += 5;
    if (draw.keyPressed(.s)) edit_sprite_image_y -= 5;
    if (draw.keyPressed(.a)) edit_sprite_image_x += 5;
    if (draw.keyPressed(.d)) edit_sprite_image_x -= 5;
    
    var tiles_cx: usize = 1;
    var tiles_cy: usize = 1;
    
    if (sprite.size == 1) {
        tiles_cx = 2;
        tiles_cy = 2;
    } else if (sprite.size == 2) {
        tiles_cx = 4;
        tiles_cy = 4;
    } else if (sprite.size == 3) {
        tiles_cx = 8;
        tiles_cy = 8;
    } else if (sprite.size == 4) {
        tiles_cx = 16;
        tiles_cy = 16;
    }
    
    const old_x = image_x;
    
    if (draw.keyDown(.e)) draw_tile_border = !draw_tile_border;
    const offset: i32 = if (draw_tile_border) 4 else 0;
    
    var j: usize = 0;
    while (j < tiles_cx) : (j += 1){
        var i: usize = 0;
        while (i < tiles_cy) : (i+= 1) {
            if (is_screen_block) {
                drawScreenBlock(sprite.tiles[i + j * tiles_cx][0..], image_x, image_y, scale, draw_tile_border);
            } else {
                drawTile(sprite.tiles[i + j * tiles_cx][0..], image_x, image_y, scale, draw_tile_border);
            }
            editTile(sprite.tiles[i + j * tiles_cx][0..], image_x, image_y, scale);
            image_x += scale * 8 + offset;
        }
        
        image_x = old_x;
        image_y += scale * 8 + offset;
    }
}

pub fn drawTile(tile: []u8, image_x: i64, image_y: i64, scale: i64, draw_border: bool) void {
    if (image_x < 300 - scale * 8) return;
    var y: usize = 0;
    while (y < 8) : (y += 1) {
        var x: usize = 0;
        while (x < 8) : (x += 1) {
            const xr = @intCast(i64, x) * scale + image_x;
            const yr = @intCast(i64, y) * scale + image_y;
            const index = x + y * 8;
            
            var x1 = xr;
            var x2 = xr + scale;
            
            if (x1 < 300) x1 = 300;
            if (x2 < 300) continue;
            
            var scale_x = @intCast(u64, x2 - x1);
            
            @call(.{.modifier = .always_inline}, draw.Buffer.fillRect, .{draw.gb, x1, yr, scale_x, @intCast(u64, scale), palette[tile[index]].toColor()});
            
        }
    }
    
    const _scale = @intCast(u64, scale) * 8 + 2;
    if (draw_border) {
        var x1 = image_x - 1;
        var x2 = x1 + @intCast(i64, _scale);
        if (x1 < 299) x1 = 299;
        if (x2 < 299) return;
        const scale_x = @intCast(u64, x2 - x1);
        
        draw.gb.drawRect(x1, image_y - 1, scale_x, _scale, Color.c(1,1,1,1));
    }
}


pub fn drawTileNotClamp(tile: []u8, image_x: i64, image_y: i64, scale: i64, draw_border: bool) void {
    var y: usize = 0;
    while (y < 8) : (y += 1) {
        var x: usize = 0;
        while (x < 8) : (x += 1) {
            const xr = @intCast(i64, x) * scale + image_x;
            const yr = @intCast(i64, y) * scale + image_y;
            const index = x + y * 8;
            
            @call(.{.modifier = .always_inline}, draw.Buffer.fillRect, .{draw.gb, xr, yr, @intCast(u64, scale), @intCast(u64, scale), palette[tile[index]].toColor()});
        }
    }
    
    const _scale = @intCast(u64, scale) * 8 + 2;
    if (draw_border) {
        draw.gb.drawRect(image_x - 1, image_y - 1, _scale, _scale, Color.c(1,1,1,1));
    }
}


pub fn drawScreenBlock(tile: []u8, image_x: i64, image_y: i64, scale: i64, draw_border: bool) void {
    const t_scale = @divExact(scale, 8);
    
    var y: usize = 0;
    while (y < 8) : (y += 1) {
        var x: usize = 0;
        while (x < 8) : (x += 1) {
            const xr = @intCast(i64, x) * scale + image_x;
            const yr = @intCast(i64, y) * scale + image_y;
            const index = tile[x + y * 8];
            
            drawTile(current_sprite.tiles[index][0..], xr, yr, t_scale, false);
        }
    }
    
    const _scale = @intCast(u64, scale) * 8 + 2;
    if (draw_border) {
        var x1 = image_x - 1;
        var x2 = x1 + @intCast(i64, _scale);
        if (x1 < 299) x1 = 299;
        if (x2 < 299) return;
        const scale_x = @intCast(u64, x2 - x1);
        draw.gb.drawRect(x1, image_y - 1, scale_x, _scale, Color.c(1,1,1,1));
    }
}

pub fn editTile(tile: []u8, image_x: i64, image_y: i64, scale: i64) void {
    const _scale = @intCast(u64, scale) * 8 + 2;
    
    var _image_x = image_x;
    var _image_x2 = image_x + @intCast(i64, _scale);
    if (_image_x2 < 300 + scale) return;
    
    if (_image_x < 300 + scale) _image_x = 300 + scale;
    const _scale_x = @intCast(u64, _image_x2 - _image_x);
    if (_scale_x <= 1) return;
    
    const mouse_on_rect = mouseOnRect(_image_x, image_y, _scale_x - 2, _scale - 2);
    
    if (mouse_on_rect) {
        const mx = @divFloor(draw.mouse_pos_x - image_x, scale);
        const my = @divFloor(draw.mouse_pos_y - image_y, scale);
        
        draw.gb.drawRect(mx * scale + image_x, my * scale + image_y, @intCast(u64, scale), @intCast(u64, scale), Color.c(1, 1, 1, 1));
        
        const index = @intCast(usize, mx + my * 8);
        if (draw.mouseButtonPressed(.left)) {
            tile[index] = selected_color_index;
        } else if (draw.mouseButtonPressed(.right)) {
            tile[index] = 0;
        } else if (draw.mouseButtonPressed(.middle)) {
            selected_color_index = tile[index];
            selected_color = palette[selected_color_index].toColor();
        }
    }
}

var file_name: draw.util.FixedSizeString(32) = .{};
var file_name2: draw.util.FixedSizeString(32) = .{};

var text_box0_selected = false;
var draw_tile_border = false;
var is_menu0_colapsed = true;
var is_menu1_colapsed = true;
var is_menu2_colapsed = true;

pub fn updateImageEditor() void {
    updatePalette(false);
    editSprite(&current_sprite, false);
    
    var pos_x: i64 = 32;
    var pos_y: i64 = 448 - 24;
    if ( menuHeadFixed("Sprite shape", pos_x, &pos_y, &is_menu0_colapsed) ) {
        selectButton("square", &current_sprite.shape, 0, pos_x, &pos_y);
    }
    if ( menuHeadFixed("Sprite Size", pos_x, &pos_y, &is_menu1_colapsed) ) {
        if (current_sprite.shape == 0) {
            selectButton("8x8", &current_sprite.size, 0, pos_x, &pos_y);
            selectButton("16x16", &current_sprite.size, 1, pos_x, &pos_y);
            selectButton("32x32", &current_sprite.size, 2, pos_x, &pos_y);
            selectButton("64x64", &current_sprite.size, 3, pos_x, &pos_y);
            selectButton("128x128", &current_sprite.size, 4, pos_x, &pos_y);
        }
    }
    if ( menuHeadFixed("File", pos_x, &pos_y, &is_menu2_colapsed) ) {
        if (button("export palette", pos_x, &pos_y)) {
            exportPalette();
        }
        if (button("save palette", pos_x, &pos_y)) {
            savePalette();
        }
        if (button("load palette", pos_x, &pos_y)) {
            loadPalette();
        }
        separator(pos_x, &pos_y);
        _ = textBox(pos_x, pos_y, 196, 16, 32, &file_name, &text_box0_selected);
        pos_y += 16;
        
        if (button("export sprite", pos_x, &pos_y)) {
            exportTile(file_name.data[0 .. file_name.len]);
        }
        if (button("save sprite", pos_x, &pos_y)) {
            saveSprite(current_sprite, file_name.data[0 .. file_name.len]) catch {
                std.debug.print("Error in save sprite\n", .{});
            };
        }
        if (button("load sprite", pos_x, &pos_y)) {
            current_sprite = loadSprite(file_name.data[0 .. file_name.len]) catch blk: {
                std.debug.print("Error in load sprite", .{});
                break :blk current_screenblock;
            };
        }
        
    }
    
}
pub fn updateScreenBlockEditor() void {
    updatePalette(true);
    editSprite(&current_screenblock, true);
    
    var pos_x: i64 = 32;
    var pos_y: i64 = 448 - 24;
    
    if ( menuHeadFixed("File", pos_x, &pos_y, &is_menu2_colapsed) ) {
        _ = textBox(pos_x, pos_y, 196, 16, 32, &file_name2, &text_box0_selected);
        pos_y += 16;
        
        if (button("export screenblock", pos_x, &pos_y)) {
            exportScreenBlock(file_name2.data[0 .. file_name2.len]);
        }
        if (button("save tileset", pos_x, &pos_y)) {
            saveSprite(current_screenblock, file_name2.data[0 .. file_name2.len]) catch {
                std.debug.print("Error in save sprite\n", .{});
            };
        }
        if (button("load tileset", pos_x, &pos_y)) {
            current_screenblock = loadSprite(file_name2.data[0 .. file_name2.len]) catch blk: {
                std.debug.print("Error in load sprite", .{});
                break :blk current_screenblock;
            };
        }
    }
}

pub fn savePalette() void {
    const dir = fs.cwd();
    
    const file = dir.createFile("palette.pal", .{ .read = true, .truncate = true }) catch return;
    defer file.close();
    
    for (palette) |clr| {
        file.writeAll(std.mem.toBytes(clr)[0..]) catch return;
    }
    
    std.debug.print("Saving palette\n", .{});
}

pub fn loadPalette() void {
    const dir = fs.cwd();
    
    const file = dir.openFile("palette.pal", .{}) catch return;
    
    for (palette) |*clr| {
        _ = file.readAll(std.mem.asBytes(clr)[0..2]) catch return;
    }
    
    selected_color = palette[selected_color_index].toColor();
    
    std.debug.print("Loading palette\n", .{});
}

pub fn saveSprite(sprite: Sprite, path: []const u8) !void {
    const cwd = fs.cwd();
    const dir = try cwd.openDir("sprites/", .{.access_sub_paths = true});
    
    const file = try dir.createFile(path, .{ .read = true, .truncate = true });
    defer file.close();
    
    try writeIntToFile(file, sprite.shape);
    try writeIntToFile(file, sprite.size);
    
    var i: usize = 0;
    while (i < 256) : (i += 1){
        try file.writeAll(sprite.tiles[i][0..]);
    }
    
    std.debug.print("Saving sprite on {s}\n", .{path});
}

pub fn loadSprite(path: []const u8) !Sprite {
    var sprite = Sprite.c();
    
    const cwd = fs.cwd();
    const dir = try cwd.openDir("sprites/", .{.access_sub_paths = true});
    
    const file = try dir.openFile(path, .{});
    defer file.close();
    
    sprite.shape = try readIntFromFile(u32, file);
    sprite.size = try readIntFromFile(u32, file);
    
    var i: usize = 0;
    while (i < 256) : (i += 1){
        _ = file.readAll(sprite.tiles[i][0..]) catch break;
    }
    
    std.debug.print("Loading sprite on {s}\n", .{path});
    return sprite;
}

fn exportPalette() void {
    const dir = fs.cwd();
    
    const file = dir.createFile("palette.h", .{ .read = true, .truncate = true }) catch return;
    defer file.close();
    
    const out = file.writer();
    
    out.print("static const u32 palette[128] __attribute__((aligned(4))) = {c}\n", .{'{'}) catch return;
    var i: usize = 0;
    while (i < 256) : (i += 2) {
        const v1: u32 = @bitCast(u16, palette[i]);
        const v2: u32 = @bitCast(u16, palette[i + 1]);
        const value: u32 = (v2 << 16) | v1;
        out.print("0x{x}, ", .{value}) catch return;
        if (i % 16 == 14) {
            out.print("\n", .{}) catch return;
        }
    }
    
    out.print("{c};\n", .{'}'}) catch return;
    
    std.debug.print("Exporting palette\n", .{});
}

fn exportTile(name: []const u8) void {
    const dir = fs.cwd();
    
    var name_buf: [128]u8 = undefined;
    var file_path = std.fmt.bufPrint(&name_buf, "exports/sprites/{s}.h", .{name}) catch return;
    
    const file = dir.createFile(file_path, .{ .read = true, .truncate = true }) catch return;
    defer file.close();
    
    const out = file.writer();
    
    const tile_count = current_sprite.tileCount();
    const size = tile_count * 64;
    
    out.print("static const u32 SP_{s}_count = {};\n", .{name, size / 4}) catch return;
    out.print("static const u32 *SP_{s} = (u32 *) (u8[{}]) {c}\n", .{name, size, '{'}) catch return;
    
    var j: usize = 0;
    while (j < tile_count) : (j += 1) {
        out.print("// Tile {}\n", .{j}) catch return;
        
        var i: usize = 0;
        while (i < 64) : (i += 1) {
            out.print("0x{x}, ", .{current_sprite.tiles[j][i]}) catch return;
        }
        
        out.print("\n", .{}) catch return;
    }
    
    out.print("{c};\n", .{'}'}) catch return;
    
    std.debug.print("Exporting tile\n", .{});
}


fn exportScreenBlock(name: []const u8) void {
    const dir = fs.cwd();
    
    
    var name_buf: [128]u8 = undefined;
    var file_path = std.fmt.bufPrint(&name_buf, "exports/screenblocks/{s}.h", .{name}) catch return;
    
    const file = dir.createFile(file_path, .{ .read = true, .truncate = true }) catch return;
    defer file.close();
    
    const out = file.writer();
    
    const tile_count = 32;
    const size = tile_count * 64;
    
    out.print("static const u32 SB_{s}_count = {};\n", .{name, size / 2}) catch return;
    out.print("static const u32 *SB_{s} = (u32 *) (u16[{}]) {c}\n", .{name, size, '{'}) catch return;
    
    var y: usize = 0;
    while (y < 32) : (y += 1) {
        var x: usize = 0;
        while (x < 32) : (x += 1) {
            const j = (x / 8) + (y / 8) * 4;
            const i = (x % 8) + (y % 8) * 8;
            out.print("0x{x}, ", .{current_screenblock.tiles[j][i]}) catch return;
        }
    }
    out.print("{c};\n", .{'}'}) catch return;
    
    std.debug.print("Exporting screenblock\n", .{});
}