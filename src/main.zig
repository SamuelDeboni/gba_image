usingnamespace @import("index.zig");

// === Global variables ===============
var main_allocator: *Allocator = undefined;
// ====================================

const EditorMode = enum {
    image,
    screen_block,
};

var editor_mode: EditorMode = .image;

fn textureFromEmbed(comptime path: []const u8) draw.Texture {
    return draw.textureFromTgaData(main_allocator, @embedFile(path)) catch @panic("Error Loading texture from embed file " ++ path ++ "\n");
}

pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    //defer _ = gpa.deinit(); // NOTE(Samuel): Dont want leak test
    
    loadPalette();
    current_sprite = loadSprite("sprite.spr") catch current_sprite;
    
    main_allocator = &gpa.allocator;
    try draw.init(&gpa.allocator, 1280, 720, start, update);
    
    savePalette();
    saveSprite(current_sprite, "sprite.spr") catch {};
}

fn start() void {
    font_small = .{
        .texture = textureFromEmbed("../assets/font_small.tga"),
        .font_size_x = 10,
        .font_size_y = 12,
        .character_spacing = 8,
    };
    
    font = .{
        .texture = textureFromEmbed("../assets/font.tga"),
        .font_size_x = 12,
        .font_size_y = 16,
        .character_spacing = 12,
    };
    
}

fn update(delta: f32) void {
    draw.gb.fillScreenWithRGBColor(10, 10, 10);
    
    if (draw.keyDown(._1)) {
        editor_mode = .image;
    } else if (draw.keyDown(._2)) {
        editor_mode = .screen_block;
    }
    
    switch (editor_mode) {
        .image => updateImageEditor(),
        .screen_block => updateScreenBlockEditor(),
    }
}
